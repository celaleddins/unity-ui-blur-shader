﻿// Based on the shader from https://stackoverflow.com/questions/29030321/unity3d-blur-the-background-of-a-ui-canvas

Shader "Custom/UIBlur" {
    Properties {
        _Size ("Blur", Range(0, 30)) = 1
        [HideInInspector] _MainTex ("Masking Texture", 2D) = "white" {}
    }

    Category {

        // We must be transparent, so other objects are drawn before this one.
        Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Opaque" }
        
        
        CGINCLUDE
        
        #include "UnityCG.cginc"
        
        struct appdata_t {
            float4 vertex : POSITION;
            float2 texcoord : TEXCOORD0;
            float4 color    : COLOR;
        };

        struct v2f {
            float4 vertex : POSITION;
            float4 uvgrab : TEXCOORD0;
            float2 uvmain : TEXCOORD1;
            float4 color    : COLOR;
        };

        sampler2D _MainTex;
        float4 _MainTex_ST;
        float _Size;

        /* TODO: Move offset calculation to vertex shader */
        v2f vert (appdata_t v)
        {
            v2f o;
            o.vertex = UnityObjectToClipPos(v.vertex);

            #if UNITY_UV_STARTS_AT_TOP
            float scale = -1.0;
            #else
            float scale = 1.0;
            #endif

            o.uvgrab.xy = (float2(o.vertex.x, o.vertex.y * scale) + o.vertex.w) * 0.5;
            o.uvgrab.zw = o.vertex.zw;

            o.uvmain = TRANSFORM_TEX(v.texcoord, _MainTex);
            
            o.color = v.color;
            return o;
        }
        
        half4 apply_blur(v2f i, half4 blurred){
            half3 resultrgb = lerp( blurred.rgb, i.color.rgb, i.color.a );
            return half4(resultrgb, tex2D(_MainTex, i.uvmain).a);
        }
        
        half4 hor_blur_pixel(sampler2D frame, float texelsize, v2f i){
            half4 sum = half4(0,0,0,0);

            #define GRABPIXEL_HOR(weight,kernelx) tex2Dproj( frame, UNITY_PROJ_COORD(float4(i.uvgrab.x + texelsize * kernelx * _Size, i.uvgrab.y, i.uvgrab.z, i.uvgrab.w))) * weight

            sum += GRABPIXEL_HOR(0.05, -4.0);
            sum += GRABPIXEL_HOR(0.09, -3.0);
            sum += GRABPIXEL_HOR(0.12, -2.0);
            sum += GRABPIXEL_HOR(0.15, -1.0);
            sum += GRABPIXEL_HOR(0.18,  0.0);
            sum += GRABPIXEL_HOR(0.15, +1.0);
            sum += GRABPIXEL_HOR(0.12, +2.0);
            sum += GRABPIXEL_HOR(0.09, +3.0);
            sum += GRABPIXEL_HOR(0.05, +4.0);
            
            return sum;
        }
        
        half4 ver_blur_pixel(sampler2D frame, float texelsize, v2f i){
            half4 sum = half4(0,0,0,0);

            #define GRABPIXEL_VER(weight,kernely) tex2Dproj( frame, UNITY_PROJ_COORD(float4(i.uvgrab.x, i.uvgrab.y + texelsize * kernely * _Size, i.uvgrab.z, i.uvgrab.w))) * weight

            sum += GRABPIXEL_VER(0.05, -4.0);
            sum += GRABPIXEL_VER(0.09, -3.0);
            sum += GRABPIXEL_VER(0.12, -2.0);
            sum += GRABPIXEL_VER(0.15, -1.0);
            sum += GRABPIXEL_VER(0.18,  0.0);
            sum += GRABPIXEL_VER(0.15, +1.0);
            sum += GRABPIXEL_VER(0.12, +2.0);
            sum += GRABPIXEL_VER(0.09, +3.0);
            sum += GRABPIXEL_VER(0.05, +4.0);
            
            return sum;
        }
        
        half4 hor_blur(sampler2D frame, float texelsize, v2f i){
            return apply_blur(i, hor_blur_pixel(frame, texelsize, i));
        }
        
        half4 ver_blur(sampler2D frame, float texelsize, v2f i){
            return apply_blur(i, ver_blur_pixel(frame, texelsize, i));
        }
        
        ENDCG

        SubShader
        {
            Cull Off
            Lighting Off
            ZWrite Off
            ZTest [unity_GUIZTestMode]
            Blend SrcAlpha OneMinusSrcAlpha
            
            // Horizontal blur
            GrabPass{"_HBlur"}
            Pass
            {          
                CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag
                #pragma fragmentoption ARB_precision_hint_fastest
                
                sampler2D _HBlur;
                float4 _HBlur_TexelSize;
                
                half4 frag( v2f i ) : COLOR
                {   
                    return hor_blur(_HBlur, _HBlur_TexelSize.x, i);
                }
                ENDCG
            }

            // Vertical blur
            GrabPass{"_VBlur"}
            Pass
            {          
                CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag
                #pragma fragmentoption ARB_precision_hint_fastest

                sampler2D _VBlur;
                float4 _VBlur_TexelSize;

                half4 frag( v2f i ) : COLOR
                {
                    return ver_blur(_VBlur, _VBlur_TexelSize.y, i);
                }
                ENDCG
            }
        }
    }
}